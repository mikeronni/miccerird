## MicceriRD.Sampling.Name_of_distribution(k,n)
Use this to generate a random sample array (via Numpy random generator) for any of Micceri's real distributions.

Arguments:

k = # of samples

n = sample size

## MicceriRD.Name_of_distribution
Use this to generate a Numpy array of the complete distributions from Micceri, 1989.

## Reference
Micceri, T. (1989). The unicorn, the normal curve, and other improbable creatures. Psychological Bulletin, 105(1), 156–166. 

https://pdfs.semanticscholar.org/2903/180261ee0d99a27cfe85cde9cf4af74923c6.pdf
